﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace Bai1
{
    internal class Program
    {
        struct Vec2
        {
            private int x, y;

            public Vec2 Add(Vec2 vec2)
            {
                int ax = x + vec2.x;
                int ay = y + vec2.y;
                return new Vec2(ax, ay);
            }

            public Vec2 Minus(Vec2 vec2)
            {
                int nx = x - vec2.x;
                int ny = y - vec2.y;
                return new Vec2(nx, ny);
            }

            public Vec2(int _x, int _y)
            {
                x = _x;
                y = _y;
            }

            public override string ToString()
            {
                return x + " " + y;
            }
        }

        private static Vec2 a = new Vec2(5, 10);
        private static Vec2 b = a.Add(new Vec2(-5, 5));
        private static Vec2 c = new Vec2(-3, 13);
        private static Vec2[] points = new Vec2[3];


        public static void Main(string[] args)
        {
            String[] content = File.ReadAllLines("D:\\VuQuangDang_TestJunior\\Bai1\\in.txt");
            for (int i = 0; i < content.Length; i++)
            {
                String[] text = content[i].Split(' ');
                points[i] = new Vec2(Int32.Parse(text[0]), Int32.Parse(text[1]));
            }

            points[0] = points[0].Add(a); //X
            points[1] = points[1].Add(b); //Y
            points[2] = points[2].Add(c); //Z
            Vec2[] results = new Vec2[3];
            results[0] = points[1].Minus(points[0]); //XY
            results[1] = points[2].Minus(points[0]); //XZ
            results[2] = points[2].Minus(points[1]); //YZ
            List<String> arr = new List<string>();
            for (int i = 0; i < results.Length; i++)
            {
                arr.Add(results[i].ToString());
                Console.WriteLine(results[i]);
            }

            File.WriteAllLines("D:\\VuQuangDang_TestJunior\\Bai1\\out.txt", arr);
            Console.ReadLine();
        }
    }
}