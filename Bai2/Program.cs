﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Bai2
{
    internal class Program
    {
        public static String Check(List<bool> input)
        {
            List<int> count = new List<int>();
            for (int i = 0; i < input.Count; i++)
            {
                if (input[i] == true)
                {
                    count.Add(0);
                    for (int j = i; j < input.Count; j++)
                    {
                        if (j == input.Count - 1)
                        {
                            i = j;
                        }

                        if (input[j] == true)
                        {
                            count[count.Count - 1]++;
                        }
                        else
                        {
                            i = j;
                            break;
                        }
                    }
                }
            }

            return String.Join(" ", count);
        }

        public static void Main(string[] args)
        {
            String[] content = File.ReadAllLines("D:\\VuQuangDang_TestJunior\\Bai2\\in.txt");
            int n = Int32.Parse(content[0]);
            List<List<bool>> matrix = new List<List<bool>>();
            for (int i = 0; i < n; i++)
            {
                matrix.Add(new List<bool>());
            }

            for (int i = 1; i < content.Length; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    String rs = content[i].Split(' ')[j];
                    if (rs == "1")
                    {
                        matrix[i - 1].Add(true);
                    }
                    else
                    {
                        matrix[i - 1].Add(false);
                    }
                }
            }

            List<String> results = new List<string>();
            List<String> input = new List<string>();
            for (int i = n - 1; i >= 0; i--)
            {
                results.Add(Check(matrix[i]));
            }

            for (int i = 0; i < n; i++)
            {
                List<bool> col = new List<bool>();
                for (int j = n - 1; j >= 0; j--)
                {
                    col.Add(matrix[j][i]);
                }
                results.Add(Check(col));
            }
            
            File.WriteAllLines("D:\\VuQuangDang_TestJunior\\Bai2\\out.txt", results);
        }
    }
}